#include <Adafruit_Fingerprint.h>
#include <Wire.h> // Library for I2C communication
#include <LiquidCrystal_I2C.h> // Library for LCD
#include <string.h>
#include <EEPROM.h>
// Wiring: SDA pin is connected to A4 and SCL pin to A5.
// Connect to LCD via I2C, default address 0x27 (A0-A2 not jumpered)
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 30, 2); // Change to (0x27,20,4) for 20x4 LCD.
#include <TimerOne.h>
SoftwareSerial mySerial(10, 11);

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

uint8_t id;

int buton1 = 5, buton2 = 4, buton3 = 3, buton4 = 2;
int registerTrue=0,voteTrue=0,realVoter,voteOp=0,admin=0;
int checkWelcomeTimer = 0,check;
int shiftWelcomePos=0;
char sirBrut[100]="         Buton 1: inregistrare; Buton 2: votare ; Buton 3: rezultate               ",sirShow[100],sirVot[100]="          1->USR 2->PNL 3->PSD 4->UDMR       ";
int votatBool[160]={0};
int psd=EEPROM.read(3),pnl=EEPROM.read(2),usr=EEPROM.read(1),udmr=EEPROM.read(4);

void setup() {
  // put your setup code here, to run once:
  lcd.init();
  lcd.backlight();

  pinMode(buton1,INPUT_PULLUP);
  pinMode(buton2,INPUT_PULLUP);
  pinMode(buton3,INPUT_PULLUP);
  pinMode(buton4,INPUT_PULLUP);
  
  Serial.begin(9600);
  while (!Serial);  // For Yun/Leo/Micro/Zero/...
  delay(100);
  Serial.println("\n\nAdafruit Fingerprint sensor enrollment");

  // set the data rate for the sensor serial port
  finger.begin(57600);

  if (finger.verifyPassword()) {
    lcd.println("Found fingerprint sensor!");
  } else {
    lcd.println("Did not find fingerprint sensor :(");
    while (1) { delay(1); }
  }
  delay(2000);
  id = EEPROM.read(0) ;
  Serial.println(id);
   //EEPROM.write(1,0);
   //EEPROM.write(2,0);
   //EEPROM.write(3,0);
   //EEPROM.write(4,0);
  Serial.println(EEPROM.length());
  initVoted();
  lcd.clear();

  lcd.print("Bine ati venit!");
  lcd.setCursor(0,1);
  lcd.print("Sistem de vot");
  delay(3000);
  lcd.clear();
  checkWelcomeTimer=1;
}
void initVoted()
{
  for(int i=0;i<100;i++)
    {
      //EEPROM.write(i+10,0);
      votatBool[i+10] = EEPROM.read(i+10);
    }
}

void checkButtons()
{
  if(digitalRead(buton1)==0){
    registerTrue=1;
  }
  if(digitalRead(buton2)==0){
    voteTrue=1;
  }
  if(digitalRead(buton3)==0){
    admin=1;
  }
}
void action()
{
  if(registerTrue==1)
  {
    lcd.clear();
    lcd.print("Buton 1 selectat!");
    delay(2000);
    lcd.clear();
    lcd.print("Inregistrare");
    lcd.setCursor(0,1);
    lcd.print("amprenta!");
    delay(3000);
    registerTrue=0;
    getFingerprintEnroll();
    id++;
    EEPROM.write(0, id);
    lcd.clear();
  }
  if(voteTrue==1)
  {
    realVoter=0;
    lcd.clear();
    lcd.print("Buton 2 selectat!");
    delay(2000);
    lcd.clear();
    lcd.print("Votare in curs..");
    lcd.setCursor(0,1);
    lcd.print("Autentificare!");
    delay(3000);
    voteTrue=0;
    uint8_t nrVote=getFingerprintID();
    //id++;
    lcd.clear();
    if(realVoter==1 && votatBool[nrVote+10]==0 ){
      lcd.print("se voteaza..");
      delay(2000);
      lcd.clear();
      lcd.print("Apasati butonul:");
      lcd.setCursor(0,1);
      votatBool[nrVote+10]=1;
      EEPROM.write(nrVote+10, 1);
        
      checkWelcomeTimer=1;
      voteOp=1;
      shiftWelcomePos=0;
      lcd.setCursor(0,1);
      lcd.print("Butonul X pentru: ");
      while(true)
      {
        welcome();
          if(digitalRead(buton1)==0){
            lcd.clear();
            usr++;
            EEPROM.write(1, usr);
            lcd.print("Votat USR!");
            delay(3000);
            break;
          }
          if(digitalRead(buton2)==0){
            lcd.clear();
            pnl++;
            EEPROM.write(2, pnl);
            lcd.print("Votat PNL!");
            delay(3000);
            break;
          }
          if(digitalRead(buton3)==0){
            lcd.clear();
            psd++;
            EEPROM.write(3, psd);
            lcd.print("Votat PSD!");
            delay(3000);
            break;
          }
          if(digitalRead(buton4)==0){
            lcd.clear();
            udmr++;
            EEPROM.write(4, udmr);
            lcd.print("Votat UDMR!");
            delay(3000);
            break;
          }

      }
      voteOp=0;
      checkWelcomeTimer=1;
      lcd.clear();
    }
    else if(votatBool[nrVote+10]==1)
    {
      lcd.clear();
      lcd.print("Votantul ");
      lcd.print(nrVote);
      lcd.print(" a");
      lcd.setCursor(0,1);
      lcd.print("votat deja! ");
      delay(3000);
      lcd.clear();
    }
  }
      if(admin==1)
      {
        admin=0;
        lcd.clear();
        lcd.print("Autentificare");
        lcd.setCursor(0,1);
        lcd.print("admin!");
        delay(3000);
        uint8_t isAdmin=getFingerprintID();

        if(isAdmin==1){
          lcd.clear();
          lcd.print("Este admin");
          delay(3000);
          lcd.clear();
          lcd.print("Rezultate...");
          delay(3000);
          lcd.clear();
          lcd.print("USR: ");
          lcd.print(usr);
          lcd.print("; PNL: ");
          lcd.print(pnl);
          lcd.setCursor(0,1);
          lcd.print("PSD: ");
          lcd.print(psd);
          lcd.print("; UDMR: ");
          lcd.print(udmr);
          delay(10000);
        }
        else {
          lcd.clear();
          lcd.print("Admin gresit!");
          delay(3000);
        }
        Timer1.attachInterrupt(ShowMessageWelcome);
        lcd.clear();
      //lcd.print("1->PNL 2->USR 3->PSD");
      }
      
    
}
uint8_t getFingerprintEnroll() {

  int p = -1;
  Serial.print("Waiting for valid finger to enroll as #"); Serial.println(id);
  lcd.clear();
  lcd.print("Puneti degetul...");// lcd.println(id);
   delay(2000);
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Imaginee luata");
      lcd.clear();
      lcd.print("Imagine luata");
      delay(2000);
      break;
    case FINGERPRINT_NOFINGER:
      Serial.println(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Imagine memorata!");
      lcd.clear();
      lcd.print("Imagine memorata!");
      delay(2000);
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }

  Serial.println("Remove finger");
  lcd.print("Ridicati degetul!");
  delay(2000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  Serial.print("ID "); Serial.println(id);
  p = -1;
  Serial.println("Place same finger again");
  lcd.clear();
  lcd.print("Puneti acelasi ");
  lcd.setCursor(0,1);
  lcd.print("deget!");
  delay(2000);
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      lcd.clear();
      lcd.print("Imagine luata");
      delay(2000);
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Imagine memorata");
      lcd.clear();
      lcd.print("Imagine memorata");
      delay(2000);
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }

  // OK converted!
  Serial.print("Creating model for #");  Serial.println(id);

  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    Serial.println("Prints matched!");
    lcd.clear();
    lcd.print("Aceeasi amprenta!");
    delay(2000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
     lcd.clear();
    Serial.println("Fingerprints did not match");
    lcd.print("Amprente");
     lcd.setCursor(0,1);
     lcd.print("diferite!");
    delay(3000);
    lcd.clear();
    lcd.print("Reincercati!");
    delay(3000);
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   

  Serial.print("ID "); Serial.println(id);
  p = finger.storeModel(id);
  if (p == FINGERPRINT_OK) {
    Serial.println("Amprenta salvata!!");
    lcd.clear();
     lcd.print("Amprenta salvata!       ");
     delay(2000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not store in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
  shiftWelcomePos=0;
}
uint8_t getFingerprintID() {
  lcd.clear();
  lcd.print("Apropiati degetul!");
  uint8_t p=1;
  while(p){
    delay(50);
    p = finger.getImage();
  }
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      lcd.clear();
      lcd.print("Imagine luata!");
      delay(2000);
      break;
    case FINGERPRINT_NOFINGER:
      Serial.println("No finger detected");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }

  // OK success!

  p = finger.image2Tz();
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Imagine converitita");
      lcd.clear();
      lcd.print("Imagine converitita!");
      delay(2000);
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }

  // OK converted!
  p = finger.fingerSearch();
  if (p == FINGERPRINT_OK) {
    Serial.println("Found a print match!");
    lcd.clear();
    realVoter=1;
    lcd.print("Amprenta valida!");
    delay(2000);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_NOTFOUND) {
    Serial.println("Did not find a match");
    lcd.clear();
    lcd.print("Amprenta invalida!");
    delay(2000);
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }


  // found a match!
  Serial.print("Found ID #"); Serial.print(finger.fingerID);
  Serial.print(" with confidence of "); Serial.println(finger.confidence);
    lcd.clear();
    lcd.print("Votant: ");
    lcd.print(finger.fingerID);
    delay(2000);
  
  return finger.fingerID;
}

// returns -1 if failed, otherwise returns ID #
int getFingerprintIDez() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return -1;

  // found a match!
  Serial.print("Found ID #"); Serial.print(finger.fingerID);
  Serial.print(" with confidence of "); Serial.println(finger.confidence);
  return finger.fingerID;
  checkWelcomeTimer=1;
  shiftWelcomePos=0;
}


void ShowMessageWelcome(void)
{
 //lcd.setCursor(0,0);
 //lcd.print(shift);
 for(int i=shiftWelcomePos;i<strlen(sirBrut)-20,i-shiftWelcomePos<20;i++)
 {
  sirShow[i-shiftWelcomePos]=sirBrut[i];
 }
 shiftWelcomePos++;
 if(shiftWelcomePos>=strlen(sirBrut)-19) shiftWelcomePos=0;
}

void VoteOp(void)
{
 //lcd.setCursor(0,0);
 //lcd.print(shift);
 for(int i=shiftWelcomePos;i<strlen(sirVot)-20,i-shiftWelcomePos<20;i++)
 {
  sirShow[i-shiftWelcomePos]=sirVot[i];
 }
 shiftWelcomePos++;
 if(shiftWelcomePos>=strlen(sirVot)-19) shiftWelcomePos=0;
}

void welcome()
{
  lcd.setCursor(0,0);
  if(checkWelcomeTimer==1){
    //lcd.print(shift);
    Timer1.initialize(500000); 
    checkWelcomeTimer=0;
    if(voteOp==1){
      Timer1.attachInterrupt(VoteOp);
      voteOp=0;
    }
    else{  
     Timer1.attachInterrupt(ShowMessageWelcome);
     }
  }
  lcd.print(sirShow);
}
void loop() {
  welcome();
  checkButtons();
  action();
}
